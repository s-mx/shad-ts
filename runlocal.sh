#!/bin/sh

exec env \
  SHADTS_SETTINGS=../env-local.cfg \
  PYTHONPATH=. \
  FLASK_APP=shadts \
  FLASK_DEBUG=1 \
  flask run
