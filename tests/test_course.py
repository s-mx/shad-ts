import pytest
import requests_mock

from .test_app import test_app
    

MOCK_DEADLINES="""
- group: Functions
  start:    01-01-2018 18:00
  deadline: 07-01-2018 18:00
  tasks:
    - task: foo
      score: 100
    - task: bar
      score: 200

- group: Classes
  start:    01-01-2018 18:00
  deadline: 07-01-2048 18:00
  tasks:
    - task: zog
      score: 100
      max_attempts: 2
    - task: zogzog
      score: 200
"""[1:]


def mock_deadlines(m):
    m.get("https://gitlab.com/slon/shad-cpp/raw/master/.deadlines.yml", text=MOCK_DEADLINES)


def test_dashboard(test_app):
    with test_app.test_client() as client:
        with requests_mock.mock() as m:
            mock_deadlines(m)

            with client.session_transaction() as session:
                session["gitlab"] = {"username": "prime"}

            rsp = client.get("/")
            assert rsp.status_code == 200
